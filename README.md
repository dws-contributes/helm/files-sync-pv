# NFS storage mapped to each project

this simple chart installs a packrat generated NFS share across many projects. 

This helm chart is currently installed in the `dws` project, so to add new projects to it, simply do the following:

- clone this repo locally
- add your new project to the values.yaml file
- helm upgrade --install filessync . --namespace=dws